import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/user/user_bloc.dart';
import 'package:task_planner/blocs/user/user_state.dart';
import 'package:task_planner/screens/auth/infor.dart';
import 'package:task_planner/screens/task/list_task.dart';
import 'package:task_planner/utils/constant.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, snapshot) {
        return SafeArea(
          child: Scaffold(
            body: SingleChildScrollView(
              child: Container(
                // padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xff3F47F4),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(20),
                          bottomLeft: Radius.circular(20),
                        ),
                      ),
                      padding: EdgeInsets.all(30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Hôm nay",
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Xin chào ${snapshot.user.username}",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.grey[200],
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => InforScreen(),
                                ),
                              );
                            },
                            child: CircleAvatar(
                              radius: 30,
                              backgroundImage: FileImage(
                                File(snapshot.user.avatar),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 20,
                      ),
                      child: Text(
                        "My tasks",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 20,
                      ),
                      children: <Widget>[
                        ListTile(
                          contentPadding: EdgeInsets.all(0),
                          title: Text("Cần làm"),
                          subtitle: Text("Các nhiệm vụ cần hoàn thành ngay"),
                          leading: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xffe46472),
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Icon(
                              Icons.history,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ListTaskScreen(
                                  type: Constants.todo,
                                ),
                              ),
                            );
                          },
                        ),
                        ListTile(
                          contentPadding: EdgeInsets.all(0),
                          title: Text("Đang thực hiện"),
                          subtitle: Text("Các nhiệm vụ đang được thực hiện"),
                          leading: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xfff8bf7c),
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Icon(
                              Icons.wb_incandescent,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ListTaskScreen(
                                  type: Constants.doing,
                                ),
                              ),
                            );
                          },
                        ),
                        ListTile(
                          contentPadding: EdgeInsets.all(0),
                          title: Text("Đã hoàn thành"),
                          subtitle: Text("Các nhiệm vụ đã hoàn thành"),
                          leading: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xff6488e4),
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Icon(
                              Icons.done,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ListTaskScreen(
                                  type: Constants.done,
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(left: 20.0),
                    //   child: Text(
                    //     "Active Projects",
                    //     style: TextStyle(
                    //       fontSize: 16,
                    //       fontWeight: FontWeight.w600,
                    //     ),
                    //   ),
                    // ),
                    // const SizedBox(
                    //   height: 20,
                    // ),
                    // GridView.builder(
                    //   shrinkWrap: true,
                    //   itemCount: 4,
                    //   physics: NeverScrollableScrollPhysics(),
                    //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    //     crossAxisCount: 2,
                    //     childAspectRatio: 1,
                    //   ),
                    //   padding: EdgeInsets.symmetric(horizontal: 10),
                    //   itemBuilder: (BuildContext context, int index) {
                    //     return Padding(
                    //       padding: const EdgeInsets.only(
                    //         right: 10,
                    //         left: 10,
                    //         bottom: 10,
                    //       ),
                    //       child: Container(
                    //         height: 40,
                    //         decoration: BoxDecoration(
                    //           color: Color(0xff309397),
                    //           borderRadius: BorderRadius.circular(20),
                    //         ),
                    //         padding: EdgeInsets.all(20),
                    //         child: Column(
                    //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //           crossAxisAlignment: CrossAxisAlignment.start,
                    //           children: <Widget>[
                    //             Text(
                    //               "2:00 - 3:00 PM",
                    //               style: TextStyle(
                    //                 color: Colors.white,
                    //                 fontSize: 16,
                    //               ),
                    //             ),
                    //             Column(
                    //               crossAxisAlignment: CrossAxisAlignment.start,
                    //               children: <Widget>[
                    //                 Text(
                    //                   "Meeting work report",
                    //                   style: TextStyle(
                    //                     color: Colors.white,
                    //                     fontSize: 14,
                    //                     fontWeight: FontWeight.w800,
                    //                   ),
                    //                 ),
                    //                 const SizedBox(
                    //                   height: 10,
                    //                 ),
                    //                 Text(
                    //                   "9 hours progress",
                    //                   style: TextStyle(
                    //                     color: Color(0xffb5d4cc),
                    //                     fontSize: 14,
                    //                     fontWeight: FontWeight.w800,
                    //                   ),
                    //                 ),
                    //               ],
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     );
                    //   },
                    // ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:task_planner/screens/calendar/calendar.dart';
import 'package:task_planner/screens/home/home.dart';
import 'package:task_planner/screens/task/add_task.dart';

class NavigatorHomeScreen extends StatefulWidget {
  @override
  _NavigatorHomeScreenState createState() => _NavigatorHomeScreenState();
}

class _NavigatorHomeScreenState extends State<NavigatorHomeScreen> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    if (index != 1) {
      setState(() {
        _selectedIndex = index;
      });
    }
  }

  var _navigation = [
    HomeScreen(),
    HomeScreen(),
    CalendarScreen(),
  ];
  var items = [
    Icon(Icons.add, size: 30),
    Icon(Icons.add, size: 30),
    Icon(Icons.compare_arrows, size: 30),
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: _navigation[_selectedIndex],
        bottomNavigationBar: Container(
          height: 60,
          width: size.width,
          color: Colors.transparent,
          child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                width: size.width,
                height: 60,
                color: Colors.transparent,
                child: BottomNavigationBar(
                  items: const <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: Icon(Icons.schedule),
                      title: Text('Dự án'),
                    ),
                    BottomNavigationBarItem(
                      title: Text(''),
                      icon: SizedBox(),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.calendar_today),
                      title: Text('Lịch trình'),
                    ),
                  ],
                  currentIndex: _selectedIndex,
                  selectedItemColor: Color(0xff3F47F4),
                  onTap: _onItemTapped,
                ),
              ),
              Positioned(
                // bottom: -40 - (75.0 - 60),
                height: 100,
                width: 100,
                child: Container(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddTaskScreen(),
                        ),
                      );
                    },
                    child: Center(
                      child: Transform.translate(
                        offset: Offset(
                          // 0,
                          // -(1 - 0.0) * 80,
                          0,
                          1,
                        ),
                        child: Material(
                          color: Color(0xff3F47F4),
                          type: MaterialType.circle,
                          child: GestureDetector(
                            onTap: () async {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => AddTaskScreen(),
                                ),
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

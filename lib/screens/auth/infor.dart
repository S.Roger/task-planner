import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/user/user_bloc.dart';
import 'package:task_planner/blocs/user/user_state.dart';
import 'package:task_planner/screens/category/category.dart';

import '../../widgets/button_item.dart';

class InforScreen extends StatelessWidget {
  Widget _elementItem({
    String title,
    Icon icon,
    Color color,
    Function onTap,
  }) {
    return ListTile(
      leading: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(12),
        ),
        child: icon,
      ),
      title: Text(title),
      trailing: IconButton(
        onPressed: onTap,
        icon: Icon(Icons.chevron_right),
      ),
      onTap: () => onTap(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, snapshot) {
      return SafeArea(
        child: Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Color(0xffF5F7F9),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(200),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back_ios),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            radius: 50,
                            backgroundColor: Colors.white,
                            backgroundImage: FileImage(
                              File(snapshot.user.avatar),
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Text(
                            snapshot.user.username,
                            style: TextStyle(
                              color: Color(0xff4F545D),
                              fontSize: 20,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            snapshot.user.description,
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  _elementItem(
                    title: "Dự án",
                    color: Color(0xffFF97BC),
                    onTap: () {},
                    icon: Icon(
                      Icons.calendar_today,
                      color: Colors.white,
                    ),
                  ),
                  _elementItem(
                    title: "Danh mục",
                    color: Color(0xff123Adf),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CategoryPage(),
                        ),
                      );
                    },
                    icon: Icon(
                      Icons.category,
                      color: Colors.white,
                    ),
                  ),
                  _elementItem(
                    title: "Lịch sử",
                    color: Color(0xffE892FF),
                    onTap: () {},
                    icon: Icon(
                      Icons.history,
                      color: Colors.white,
                    ),
                  ),
                  _elementItem(
                    title: "Cài đặt",
                    color: Color(0xff3CE354),
                    onTap: () {},
                    icon: Icon(
                      Icons.settings,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              // Padding(
              //   padding: const EdgeInsets.all(20.0),
              //   child: ButtonItem(
              //     title: "Create User",
              //     onTap: () async {
              //       print("On tap");

              //       await DatabaseHelper.instance.insert({
              //         "username": "Roger",
              //         "email": "admin@gmail.com",
              //         "password": "123456",
              //         "avatar":
              //             "https://am23.mediaite.com/tms/cnt/uploads/2019/05/chris-evans-1-1200x722.jpg"
              //       }, "user").then((value) => print(value));
              //     },
              //   ),
              // ),
              // Padding(
              //   padding: const EdgeInsets.all(20.0),
              //   child: ButtonItem(
              //     title: "Đăng xuất",
              //     onTap: () {
              //       print("On tap");
              //     },
              //   ),
              // ),
            ],
          ),
        ),
      );
    });
  }
}

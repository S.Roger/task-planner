import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:task_planner/blocs/user/user_bloc.dart';
import 'package:task_planner/blocs/user/user_event.dart';
import 'package:task_planner/blocs/user/user_state.dart';
import 'package:task_planner/database/database_helper.dart';
import 'package:task_planner/models/user.dart';
import 'package:task_planner/screens/navigator.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/widgets/button_item.dart';
import 'package:task_planner/widgets/toast_error.dart';
// import

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController _username = TextEditingController();
  TextEditingController _infor = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  PickedFile image;
  File _localImage;
  void _pickImage(ImageSource source) async {
    final pickImage = await ImagePicker().getImage(source: source);
    if (pickImage != null) {
      setState(() {
        image = pickImage;
      });
      Navigator.pop(context);
    }
  }

  handleForm() async {
    if (_formKey.currentState.validate()) {
      if (image != null) {
        final directory = await getApplicationDocumentsDirectory();
        var fileName = "avatar.png";
        File avatar = File(image.path);
        File localImage = await avatar.copy('${directory.path}/$fileName');
        UserModel user = UserModel.fromJson({
          "username": _username.text,
          "description": _infor.text,
          "avatar": localImage.path,
        });
        await DatabaseHelper.instance.insert(user.toJson(), "user");
        BlocProvider.of<UserBloc>(context).add(UserCheckLogin());
      } else {
        FlutterToast(context).showToast(
            child: ToastErrror(
          text: "Vui lòng chọn ảnh",
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocBuilder<UserBloc, UserState>(builder: (context, data) {
      if (data is UserInitial) {
        BlocProvider.of<UserBloc>(context).add(UserCheckLogin());
      }
      if (data is UserLoginSuccess) {
        return NavigatorHomeScreen();
      }
      if (data is UserLoginError) {
        return Scaffold(
          body: Form(
            key: _formKey,
            child: Stack(
              children: <Widget>[
                Container(
                  height: size.height,
                ),
                Container(
                  height: size.height / 2,
                  decoration: BoxDecoration(
                    color: Color(ColorUtils.blueColor),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30),
                    ),
                  ),
                ),
                Positioned(
                  top: size.height / 6,
                  left: 20,
                  right: 20,
                  child: Container(
                    height: size.height * 4 / 6,
                    padding: EdgeInsets.all(20),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: SingleChildScrollView(
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                Container(
                                  height: 100,
                                  width: 100,
                                  child: CircleAvatar(
                                    maxRadius: 60,
                                    backgroundImage: image == null
                                        ? AssetImage("assets/images/avatar.png")
                                        : FileImage(
                                            File(image.path),
                                          ),
                                  ),
                                ),
                                Positioned(
                                  top: 50,
                                  child: GestureDetector(
                                    onTap: () =>
                                        _settingModalBottomSheet(context),
                                    child: Container(
                                      height: 50,
                                      width: 100,
                                      decoration: BoxDecoration(
                                        color: Colors.black38,
                                        // borderRadius: BorderRadius.circular(20),
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(60),
                                          bottomRight: Radius.circular(60),
                                        ),
                                      ),
                                      child: Icon(Icons.camera_alt),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            elementInput(
                              title: "Tên hiển thị",
                              icon: Icon(Icons.person),
                              controller: _username,
                            ),
                            elementInput(
                              title: "Thông tin",
                              icon: Icon(Icons.info),
                              controller: _infor,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            ButtonItem(
                              title: "Lưu",
                              onTap: handleForm,
                            ),
                            if (_localImage != null)
                              Image.file(File(_localImage.path))
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    });
  }

  Widget elementInput({
    String title,
    TextEditingController controller,
    Icon icon,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            color: Colors.grey,
            fontWeight: FontWeight.w800,
            fontSize: 16,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        TextFormField(
          controller: controller,
          validator: (value) {
            String result = value.trim();
            if (result == "" || result == null) {
              return "Vui lòng nhập trường này";
            }
            return null;
          },
          style: TextStyle(
            color: Colors.black,
          ),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            // hintText: hideText,
            labelStyle: TextStyle(
              color: Colors.red,
              fontSize: 14,
            ),
            hintStyle: TextStyle(
              color: Colors.black,
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
              borderRadius: BorderRadius.circular(30),
            ),
            prefixIcon: icon,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
              borderRadius: BorderRadius.circular(30),
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(Icons.camera_alt),
                  title: Text('Image'),
                  onTap: () => _pickImage(ImageSource.camera),
                ),
                ListTile(
                  leading: Icon(Icons.photo),
                  title: Text('Gallery'),
                  onTap: () => _pickImage(ImageSource.gallery),
                ),
              ],
            ),
          );
        });
  }
}

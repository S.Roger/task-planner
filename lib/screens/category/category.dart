import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/category/category_bloc.dart';
import 'package:task_planner/blocs/category/category_event.dart';
import 'package:task_planner/blocs/category/category_state.dart';
import 'package:task_planner/screens/category/category_add.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/widgets/back_item.dart';

import '../../utils/utils.dart';

class CategoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(builder: (context, data) {
      if (data is CategoryInitialized) {
        BlocProvider.of<CategoryBloc>(context).add(CategoryList());
        return Scaffold(
          appBar: AppBar(
            title: Text("List Category"),
            centerTitle: true,
            backgroundColor: Color(
              ColorUtils.blueColor,
            ),
            leading: BackItem(),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CategoryAddPage(),
                    ),
                  );
                },
              ),
            ],
          ),
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: Text("Danh sách danh mục"),
          centerTitle: true,
          backgroundColor: Color(
            ColorUtils.blueColor,
          ),
          leading: BackItem(),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CategoryAddPage(),
                  ),
                );
              },
            ),
          ],
        ),
        body: GridView.builder(
          padding: EdgeInsets.all(10),
          itemCount: data.props.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
          ),
          itemBuilder: (BuildContext context, int index) {
            return Card(
              color:
                  Utils.stringToColor(data.props[index]?.color ?? "0xff6ACBFE"),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: GridTile(
                footer: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Do you want delete?"),
                                  actions: [
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("Cancel"),
                                    ),
                                    FlatButton(
                                      onPressed: () {
                                        BlocProvider.of<CategoryBloc>(context)
                                            .add(CategoryDelete(
                                                data.props[index]));
                                        Navigator.pop(context);
                                      },
                                      child: Text("Ok"),
                                    )
                                  ],
                                );
                              });
                        },
                      ),
                      GestureDetector(
                        child: Image.asset(
                          "assets/images/icon_edit.png",
                          color: Colors.white,
                          width: 20,
                          height: 20,
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  CategoryAddPage(
                                category: data.props[index],
                              ),
                            ),
                          );
                        },
                      )
                    ],
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    data.props[index]?.name ?? "",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w800,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      );
    });
  }
}

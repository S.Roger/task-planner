import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:task_planner/blocs/category/category_bloc.dart';
import 'package:task_planner/blocs/category/category_event.dart';
import 'package:task_planner/blocs/user/user_bloc.dart';
import 'package:task_planner/models/category.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/widgets/back_item.dart';
import 'package:task_planner/widgets/input_element.dart';
import 'package:task_planner/widgets/toast_success.dart';

import '../../models/category.dart';
import '../../utils/utils.dart';
import '../../widgets/button_item.dart';

class CategoryAddPage extends StatefulWidget {
  final CategoryModel category;

  const CategoryAddPage({Key key, this.category}) : super(key: key);

  @override
  _CategoryAddPageState createState() => _CategoryAddPageState();
}

class _CategoryAddPageState extends State<CategoryAddPage> {
  TextEditingController _name = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Color pickerColor = Color(0xff443a49);
  Color currentColor = Color(0xff443a49);

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() {
    if (widget.category != null) {
      _name.text = widget.category.name;
      setState(() {
        currentColor = Color(int.parse(widget.category.color));
        pickerColor = Color(int.parse(widget.category.color));
      });
    }
  }

  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            widget.category == null ? "Tạo mới danh mục" : "Cập nhật danh mục",
          ),
          backgroundColor: Color(ColorUtils.blueColor),
          centerTitle: true,
          leading: BackItem(),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Form(
              key: _formKey,
              child: Container(
                height: MediaQuery.of(context).size.height - 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    InputElement(
                      title: "Danh mục",
                      hideText: "Tên danh mục",
                      controller: _name,
                      validateFunction: (value) {
                        if (value == null || value == "") {
                          return "Bạn vui lòng nhập tên của danh mục";
                        }
                        return null;
                      },
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Color",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              width: 100,
                              height: 45,
                              decoration: BoxDecoration(
                                color: pickerColor != null
                                    ? pickerColor
                                    : Color(0xff443a49),
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: IconButton(
                                icon:
                                    Image.asset("assets/images/icon_edit.png"),
                                onPressed: () {
                                  _showDialog();
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    // Container(
                    //   width: MediaQuery.of(context).size.width,
                    //   height: 50,
                    //   child: FlatButton(
                    //     shape: RoundedRectangleBorder(
                    //       borderRadius: BorderRadius.circular(10),
                    //     ),
                    //     onPressed: handleForm,
                    //     child: Text(
                    //       (widget.category == null
                    //               ? "Create Category"
                    //               : "Update Category")
                    //           .toUpperCase(),
                    //       style: TextStyle(
                    //         color: Colors.white,
                    //       ),
                    //     ),
                    //     color: Color(ColorUtils.blueColor),
                    //   ),
                    // ),
                    ButtonItem(
                      title: (widget.category == null ? "Tạo mới" : "Cập nhật"),
                      onTap: handleForm,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void handleForm() {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      if (widget.category != null) {
        widget.category.name = _name.text;
        widget.category.color = Utils.colorToString(currentColor);
        BlocProvider.of<CategoryBloc>(context).add(
          CategoryEdit(widget.category),
        );
        FlutterToast(context).showToast(
            child: ToastSuccess(
          text: "Cập nhật thành công",
        ));
      } else {
        Map<String, dynamic> data = {
          "name": _name.text,
          "color": Utils.colorToString(currentColor),
        };
        BlocProvider.of<CategoryBloc>(context).add(
          CategoryAdd(
            CategoryModel.fromJson(data),
          ),
        );
        FlutterToast(context).showToast(
            child: ToastSuccess(
          text: "Tạo mới thành công",
        ));
      }
      Navigator.pop(context);
    }
  }

  void _showDialog() {
    showDialog(
      child: AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(32.0),
          ),
        ),
        title: const Text('Pick a color!'),
        content: SingleChildScrollView(
          child: BlockPicker(
            pickerColor: currentColor,
            onColorChanged: changeColor,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            color: Color(ColorUtils.blueColor),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: const Text('Choose'),
            onPressed: () {
              setState(() => currentColor = pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      context: context,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:task_planner/blocs/task/task_bloc.dart';
import 'package:task_planner/blocs/task/task_event.dart';
import 'package:task_planner/blocs/task/task_state.dart';
import 'package:task_planner/database/database_helper.dart';
import 'package:task_planner/models/task.dart';
import 'package:task_planner/screens/calendar/widgets/timeline_item.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/utils/utils.dart';
import 'package:task_planner/widgets/toast_success.dart';

class CalendarScreen extends StatefulWidget {
  @override
  _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  List<TaskModel> tasks = [];

  int _index = 0;
  Widget elementTab({
    Size size,
    String title,
    String des,
    int index,
    Function onTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 100,
        width: size.width / 5,
        decoration: BoxDecoration(
          color:
              index == _index ? Color(ColorUtils.blueColor) : Color(0xfff5f6fa),
          borderRadius: BorderRadius.circular(10),
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                color: index == _index ? Colors.white : Colors.black,
                fontSize: 22,
              ),
            ),
            Text(
              des,
              style: TextStyle(
                color: Colors.grey,
              ),
            )
          ],
        ),
      ),
    );
  }

  void initTask(String date) async {
    List<Map<String, dynamic>> result =
        await DatabaseHelper.instance.queryAllWhere("task", date);
    List<TaskModel> list = [];
    result.forEach((element) {
      list.add(TaskModel.fromJson(element));
    });
    if (mounted) {
      setState(() {
        tasks = list;
      });
    }
  }

  void updateStatus(int status, TaskModel task) async {
    print(task.id);
    final result = await DatabaseHelper.instance.update({
      "title": task.title,
      "date": task.date,
      "startTime": task.startTime,
      "endTime": task.endTime,
      "category_id": task.categoryId,
      "description": task.description,
      "id": task.id,
      "status": status,
    }, "task");
    if (result == 1) {
      FlutterToast(context).showToast(
          child: ToastSuccess(
        text: "Cập nhật thành công",
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocBuilder<TaskBloc, TaskState>(builder: (context, data) {
      if (data is TaskStateInitial) {
        BlocProvider.of<TaskBloc>(context).add(TaskCalendarList());
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      if (data.calendar.length > 0) initTask(data.calendar[_index].date);
      return DefaultTabController(
        length: data.calendar.length,
        initialIndex: _index,
        child: Scaffold(
          backgroundColor: Color(0xfff5f6fa),
          // backgroundColor: Colors.red,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(size.height / 4),
            child: Container(
              // color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    width: size.width,
                    height: size.height / 10,
                    color: Color(0xfff5f6fa),
                    // color: Colors.black,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          Utils.timeToday(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    width: size.width,
                    height: size.height * 0.15,
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 15,
                        ),
                        Center(
                          child: TabBar(
                            labelPadding: EdgeInsets.only(left: 10, right: 10),
                            labelStyle: TextStyle(
                              color: Colors.red,
                            ),
                            unselectedLabelStyle: TextStyle(
                              color: Colors.black,
                            ),
                            isScrollable: true,
                            indicatorColor: Colors.white,
                            tabs: [
                              for (int i = 0; i < data.calendar.length; i++)
                                elementTab(
                                  size: size,
                                  title: data.calendar[i].date.split("/")[0],
                                  des: Utils.dayOfWeek(data.calendar[i].date),
                                  index: i,
                                  onTap: () {
                                    initTask(data.calendar[i].date);
                                    setState(() {
                                      _index = i;
                                    });
                                  },
                                )
                            ],
                          ),
                        ),
                        // Divider(
                        //   thickness: 1,
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          body: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            color: Colors.white,
            child: ListView.separated(
              itemCount: tasks.length,
              itemBuilder: (BuildContext context, int index) {
                return TimelineItem(
                  task: tasks[index],
                  onTap: (value) {
                    int status = value.value;
                    updateStatus(status, tasks[index]);
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(height: 10);
              },
            ),
          ),
        ),
      );
    });
  }
}

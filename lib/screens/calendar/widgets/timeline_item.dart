import 'package:flutter/material.dart';
import 'package:task_planner/models/task.dart';
import 'package:task_planner/screens/task/detail_task.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/utils/constant.dart';
import 'package:task_planner/utils/utils.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TimelineItem extends StatelessWidget {
  const TimelineItem({
    Key key,
    this.task,
    this.onTap,
  }) : super(key: key);

  final TaskModel task;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return TimelineTile(
      alignment: TimelineAlign.manual,
      lineX: 0.3,
      indicatorStyle: IndicatorStyle(
        width: 30,
        color: statusColor(task.status),
        padding: const EdgeInsets.all(8),
        iconStyle: IconStyle(
          color: Colors.white,
          iconData: task.status == Constants.done
              ? Icons.check
              : task.status == Constants.doing
                  ? Icons.wb_incandescent
                  : Icons.history,
        ),
      ),
      rightChild: GestureDetector(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return DetailTask(
              taskModel: task,
            );
          }));
        },
        child: Container(
          padding: EdgeInsets.all(10),
          constraints: const BoxConstraints(
            minHeight: 120,
          ),
          decoration: BoxDecoration(
            color: Utils.stringToColor(task.color),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                task.title,
                style: TextStyle(
                  // color: Color(ColorUtils.blueColor),
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                ),
              ),
              Text(
                task.description,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      left: 10,
                      right: 10,
                      top: 5,
                      bottom: 5,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.white,
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Text(
                      task.name,
                      style: TextStyle(
                        color: Utils.stringToColor(task.color),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: PopupMenuButton<Choice>(
                      onSelected: onTap,
                      color: Colors.white,
                      itemBuilder: (BuildContext context) {
                        return choices.map((Choice choice) {
                          return PopupMenuItem<Choice>(
                            value: choice,
                            child: Text(choice.title),
                          );
                        }).toList();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      leftChild: Center(
        child: Text(
          task.startTime + " - " + task.endTime,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 14,
          ),
        ),
      ),
    );
  }

  Color statusColor(int status) {
    switch (status) {
      case Constants.todo:
        return ColorUtils.todoColor;
      case Constants.doing:
        return ColorUtils.doingColor;
      case Constants.done:
        return ColorUtils.doneColor;
      default:
    }
    return ColorUtils.todoColor;
  }
}

class Choice {
  const Choice({this.title, this.value});

  final String title;
  final int value;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Todo', value: 0),
  const Choice(title: 'Doing', value: 1),
  const Choice(title: 'Done', value: 2),
];

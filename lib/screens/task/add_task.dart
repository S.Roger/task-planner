import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:task_planner/blocs/category/category_bloc.dart';
import 'package:task_planner/blocs/category/category_event.dart';
import 'package:task_planner/blocs/category/category_state.dart';
import 'package:task_planner/blocs/task/task_bloc.dart';
import 'package:task_planner/blocs/task/task_event.dart';
import 'package:task_planner/database/database_helper.dart';
import 'package:task_planner/models/category.dart';
import 'package:task_planner/models/task.dart';
import 'package:task_planner/screens/task/widgets/select_category.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/utils/utils.dart';
import 'package:task_planner/widgets/back_item.dart';
import 'package:task_planner/widgets/button_item.dart';
import 'package:task_planner/widgets/input_element.dart';
import 'package:task_planner/widgets/toast_error.dart';
import 'package:task_planner/widgets/toast_success.dart';
import '../../models/category.dart';
import '../../utils/utils.dart';

class AddTaskScreen extends StatefulWidget {
  final TaskModel task;

  const AddTaskScreen({Key key, this.task}) : super(key: key);
  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  DateTime selectedDate = DateTime.now();
  TimeOfDay startTime = TimeOfDay.now();
  TimeOfDay endTime = TimeOfDay.now();
  CategoryModel _selectCategory;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _title = TextEditingController();
  TextEditingController _description = TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.task != null) {
      initForm();
    }
  }

  void initForm() {
    _title.text = widget.task.title;
    _description.text = widget.task.description;
    selectedDate = DateTime(
      int.parse(widget.task.date.split("/")[2]),
      int.parse(widget.task.date.split("/")[1]),
      int.parse(widget.task.date.split("/")[0]),
    );
    startTime = TimeOfDay(
      hour: int.parse((widget.task.startTime.split(":")[0])),
      minute: int.parse((widget.task.startTime.split(":")[1])),
    );
    endTime = TimeOfDay(
      hour: int.parse((widget.task.endTime.split(":")[0])),
      minute: int.parse((widget.task.endTime.split(":")[1])),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<void> _selectTime(BuildContext context, bool start) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: start ? startTime : endTime,
    );
    if (picked != null && (start && picked != startTime) ||
        (!start && picked != endTime)) {
      if (start) {
        setState(() {
          startTime = picked;
        });
      } else {
        setState(() {
          endTime = picked;
        });
      }
    }
  }

  Widget elementTime({
    Size size,
    String title,
    bool start = true,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          width: size.width / 2 - 80,
          height: 46,
          padding: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                Utils.getTime(start ? startTime : endTime),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                ),
              ),
              IconButton(
                onPressed: () {
                  _selectTime(context, start);
                },
                icon: Icon(Icons.access_time),
                iconSize: 20,
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocBuilder<CategoryBloc, CategoryState>(builder: (context, data) {
      if (data is CategoryInitialized) {
        BlocProvider.of<CategoryBloc>(context).add(CategoryList());
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.task == null ? "Create Task" : "Update Task"),
            centerTitle: true,
            backgroundColor: Color(
              ColorUtils.blueColor,
            ),
            leading: BackItem(),
          ),
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      }
      return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          appBar: AppBar(
            leading: BackItem(),
            backgroundColor: Color(ColorUtils.blueColor),
            elevation: 0,
            title: Text(widget.task == null ? "Create Task" : "Update Task"),
            centerTitle: true,
          ),
          body: Form(
            key: _formKey,
            child: Container(
              color: Colors.transparent,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Color(0xff3F47F4),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InputElement(
                              title: "Title",
                              dark: true,
                              controller: _title,
                              validateFunction: (value) {
                                if (value == null || value == "") {
                                  return "Enter your title";
                                }
                                return null;
                              },
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Date",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  width: size.width / 2 - 45,
                                  height: 46,
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.white,
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        Utils.getDateTime(selectedDate),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          _selectDate(context);
                                        },
                                        icon: Icon(Icons.date_range),
                                        color: Colors.white,
                                        iconSize: 20,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              elementTime(
                                title: "Start time",
                                size: size,
                              ),
                              elementTime(
                                title: "End time",
                                size: size,
                                start: false,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          if (data.props != null)
                            SelectCategory(
                              categories: data.props,
                              category: widget.task == null
                                  ? _selectCategory
                                  : getSelectData(
                                      data.props, widget.task.categoryId),
                              onChange: (value) {
                                setState(() {
                                  _selectCategory = value;
                                });
                              },
                            ),
                          InputElement(
                            title: "Description",
                            controller: _description,
                            maxLine: 5,
                            validateFunction: (value) {
                              if (value == null || value == "") {
                                return "Enter your description";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          ButtonItem(
                            title: widget.task == null
                                ? "Create Task"
                                : "Update Task",
                            onTap: handleForm,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }

  CategoryModel getSelectData(List<CategoryModel> list, int id) {
    CategoryModel select = list.where((element) => element.id == id).first;
    return select;
  }

  void handleForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      if (_selectCategory == null && widget.task == null) {
        FlutterToast(context).showToast(
          child: ToastErrror(text: "Vui lòng chọn danh mục"),
        );
      } else {
        var data = {
          "title": _title.text,
          "date": Utils.getDateTime(selectedDate),
          "startTime": Utils.getTime(startTime),
          "endTime": Utils.getTime(endTime),
          "category_id": _selectCategory?.id ?? widget.task.categoryId,
          "description": _description.text,
          "status": widget.task == null ? 0 : widget.task.status,
        };
        if (widget.task == null) {
          BlocProvider.of<TaskBloc>(context).add(
            TaskAdd(
              TaskModel.fromJson(data),
            ),
          );
          FlutterToast(context)
              .showToast(child: ToastSuccess(text: "Create task successfully"));
          Navigator.pop(context);
        } else {
          data['id'] = widget.task.id;
          await DatabaseHelper.instance.update(data, "task");
          FlutterToast(context)
              .showToast(child: ToastSuccess(text: "Update task successfully"));
          Navigator.pop(context);
          Navigator.pop(context);
        }
      }
    }
  }
}

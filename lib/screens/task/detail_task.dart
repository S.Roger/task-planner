import 'package:flutter/material.dart';
import 'package:task_planner/models/task.dart';
import 'package:task_planner/screens/task/add_task.dart';
import 'package:task_planner/utils/color.dart';
import 'package:task_planner/utils/constant.dart';
import 'package:task_planner/utils/utils.dart';
import 'package:task_planner/widgets/back_item.dart';

class DetailTask extends StatelessWidget {
  final TaskModel taskModel;

  const DetailTask({
    Key key,
    this.taskModel,
  }) : super(key: key);

  Widget elmentItem({
    Icon icon,
    String title,
    Widget right,
  }) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                icon,
                const SizedBox(width: 10),
                right != null ? right : Expanded(child: Text(title)),
              ],
            ),
          ),
          Divider()
        ],
      ),
    );
  }

  String getStatus(int status) {
    switch (status) {
      case Constants.todo:
        return "Todo";
        break;
      case Constants.doing:
        return "Doing";
        break;
      default:
        return "Done";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Task"),
        backgroundColor: Color(ColorUtils.blueColor),
        centerTitle: true,
        leading: BackItem(),
        actions: <Widget>[
          GestureDetector(
            child: Row(
              children: <Widget>[
                Image.asset(
                  "assets/images/icon_edit.png",
                  color: Colors.white,
                  width: 20,
                  height: 20,
                ),
                SizedBox(width: 20),
              ],
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => AddTaskScreen(
                    task: taskModel,
                  ),
                ),
              );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            elmentItem(
              icon: Icon(
                Icons.title,
                size: 20,
              ),
              title: taskModel.title,
            ),
            elmentItem(
              icon: Icon(
                Icons.calendar_today,
                size: 20,
              ),
              title: taskModel.date,
            ),
            elmentItem(
              icon: Icon(
                Icons.access_time,
                size: 20,
              ),
              title: taskModel.startTime + " - " + taskModel.endTime,
            ),
            elmentItem(
              icon: Icon(
                Icons.sentiment_satisfied,
                size: 20,
              ),
              title: getStatus(taskModel.status),
            ),
            elmentItem(
              icon: Icon(
                Icons.category,
                size: 20,
              ),
              // title: taskModel.name,
              right: Container(
                padding: EdgeInsets.only(
                  top: 5,
                  bottom: 5,
                  right: 10,
                  left: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Utils.stringToColor(taskModel.color),
                ),
                child: Text(
                  taskModel.name,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            elmentItem(
              icon: Icon(
                Icons.description,
                size: 20,
              ),
              title: taskModel.description,
            ),
          ],
        ),
      )),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:task_planner/database/database_helper.dart';
import 'package:task_planner/models/task.dart';
import 'package:task_planner/utils/constant.dart';
import 'package:task_planner/widgets/item_project.dart';
import 'package:task_planner/widgets/toast_success.dart';

class ListTaskScreen extends StatefulWidget {
  final int type;

  const ListTaskScreen({Key key, this.type}) : super(key: key);
  @override
  _ListTaskScreenState createState() => _ListTaskScreenState();
}

class _ListTaskScreenState extends State<ListTaskScreen> {
  String _title = "";
  List<TaskModel> _list = [];
  @override
  void initState() {
    super.initState();
    initTitle();
    initData();
  }

  void initTitle() {
    String title;
    if (widget.type == Constants.todo) {
      title = "Todo Task";
    } else if (widget.type == Constants.doing) {
      title = "Doing Task";
    } else {
      title = "Done Task";
    }
    setState(() {
      _title = title;
    });
  }

  void updateStatus(int status, TaskModel task) async {
    print(task.id);
    final result = await DatabaseHelper.instance.update({
      "title": task.title,
      "date": task.date,
      "startTime": task.startTime,
      "endTime": task.endTime,
      "category_id": task.categoryId,
      "description": task.description,
      "id": task.id,
      "status": status,
    }, "task");
    if (result == 1) {
      FlutterToast(context).showToast(
          child: ToastSuccess(
        text: "Cập nhật thành công",
      ));
      initData();
    }
  }

  void initData() async {
    List<Map<String, dynamic>> result =
        await DatabaseHelper.instance.getTaskByDay(widget.type);
    List<TaskModel> list = [];
    result.forEach((element) {
      list.add(TaskModel.fromJson(element));
    });
    print(list);
    if (mounted) {
      setState(() {
        _list = list;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff3F47F4),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text(
          _title,
        ),
        centerTitle: true,
      ),
      body: Container(
        child: ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.all(20),
          // physics: NeverScrollableScrollPhysics(),
          itemBuilder: (_, index) {
            final item = _list[index];
            return Dismissible(
              key: Key(item.id.toString()),
              confirmDismiss: (direction) {
                return showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text("Do you want delete?"),
                      actions: [
                        FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
                        ),
                        FlatButton(
                          onPressed: () async {
                            await DatabaseHelper.instance
                                .delete(_list[index].id, "task");
                            FlutterToast(context).showToast(
                              child: ToastSuccess(text: "Delete successfully"),
                            );
                            Navigator.pop(context);
                            initData();
                          },
                          child: Text("Ok"),
                        )
                      ],
                    );
                  },
                );
              },
              child: ItemProject(
                bg: Color(0xffFFE6E7),
                task: _list[index],
                onTap: (value) {
                  int status = value.value;
                  updateStatus(status, _list[index]);
                },
              ),
            );
          },
          separatorBuilder: (_, index) {
            return SizedBox(
              height: 20,
            );
          },
          itemCount: _list.length,
        ),
      ),
    );
  }
}

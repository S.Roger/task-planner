import 'package:flutter/material.dart';
import 'package:task_planner/models/category.dart';
import 'package:task_planner/screens/category/category_add.dart';

class SelectCategory extends StatelessWidget {
  final List<CategoryModel> categories;
  final CategoryModel category;
  final Function onChange;
  const SelectCategory({
    Key key,
    this.categories,
    this.category,
    this.onChange,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Category",
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            DropdownButton<CategoryModel>(
              hint: Text("Select category"),
              value: category,
              onChanged: (CategoryModel newValue) {
                onChange(newValue);
              },
              items: categories.map((CategoryModel user) {
                return DropdownMenuItem<CategoryModel>(
                  value: user,
                  child: Text(user.name,
                      style: TextStyle(
                        color: Colors.black,
                        // fontWeight: FontWeight.bold,
                      )),
                );
              }).toList(),
            ),
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => CategoryAddPage(),
                  ),
                );
              },
            )
          ],
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}

class UserModel {
  int id;
  String username;
  String description;
  String avatar;

  UserModel({this.id, this.username, this.description, this.avatar});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    description = json['description'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['description'] = this.description;
    data['avatar'] = this.avatar;
    return data;
  }
}

import 'category.dart';

class TaskModel {
  int id;
  String title;
  String date;
  String startTime;
  String endTime;
  String description;
  CategoryModel category;
  int categoryId;
  int userId;
  String name;
  String color;
  int status;

  TaskModel({
    this.id,
    this.title,
    this.date,
    this.startTime,
    this.endTime,
    this.description,
    this.category,
    this.color,
    this.name,
    this.status,
  });

  TaskModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    date = json['date'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    description = json['description'];
    category = json['category'] != null
        ? new CategoryModel.fromJson(json['category'])
        : null;
    categoryId = json['category_id'];
    userId = json['user_id'];
    name = json['name'];
    color = json['color'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['date'] = this.date;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['description'] = this.description;
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    data['category_id'] = this.categoryId;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['color'] = this.color;
    data['status'] = this.status;
    return data;
  }
}

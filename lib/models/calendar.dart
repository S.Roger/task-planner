class CalendarModel {
  String date;
  String day;

  CalendarModel({this.date, this.day});

  CalendarModel.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    day = json['day'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['day'] = this.day;
    return data;
  }
}

import 'package:flutter/material.dart';

class InputElement extends StatelessWidget {
  const InputElement({
    Key key,
    this.title,
    this.hideText = "",
    this.require = true,
    this.controller,
    this.dark = false,
    this.maxLine = 1,
    this.validateFunction,
  }) : super(key: key);

  final String title;
  final String hideText;
  final bool require;
  final TextEditingController controller;
  final bool dark;
  final int maxLine;
  final String Function(String) validateFunction;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: dark ? Colors.white : Colors.black,
              ),
            ),
            const SizedBox(
              width: 2,
            ),
            if (require)
              Text(
                "*",
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 16,
                ),
              ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        TextFormField(
          controller: controller,
          maxLines: maxLine,
          validator: (value) {
            return validateFunction(value);
          },
          style: TextStyle(
            color: dark ? Colors.white : Colors.black,
          ),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            hintText: hideText,
            labelStyle: TextStyle(
              color: Colors.red,
              fontSize: 14,
            ),
            hintStyle: TextStyle(
              color: Colors.black,
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: dark ? Colors.white : Colors.black,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            // suffixIcon: icon,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: dark ? Colors.white : Colors.black,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:task_planner/utils/color.dart';

class ButtonItem extends StatelessWidget {
  const ButtonItem({
    Key key,
    this.title,
    this.onTap,
    this.color,
  }) : super(key: key);

  final String title;
  final Function onTap;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(ColorUtils.blueColor),
        borderRadius: BorderRadius.circular(10),
      ),
      width: MediaQuery.of(context).size.width,
      height: 45,
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        onPressed: onTap,
        child: Text(
          title.toUpperCase(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        color: color == null ? Color(ColorUtils.blueColor) : color,
      ),
    );
  }
}

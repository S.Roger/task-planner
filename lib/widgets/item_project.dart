import 'package:flutter/material.dart';
import 'package:task_planner/models/task.dart';
import 'package:task_planner/screens/calendar/widgets/timeline_item.dart';
import 'package:task_planner/screens/task/detail_task.dart';
import 'package:task_planner/utils/utils.dart';

class ItemProject extends StatelessWidget {
  final Color bg;
  final TaskModel task;
  final Function onTap;
  const ItemProject({
    Key key,
    this.bg,
    this.task,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return DetailTask(
            taskModel: task,
          );
        }));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(
          top: 20,
          right: 20,
          left: 20,
          bottom: 0,
        ),
        decoration: BoxDecoration(
          color: Utils.stringToColor(task.color),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  task.title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  task.startTime + "-" + task.endTime,
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.timer,
                  size: 15,
                  color: Colors.white,
                ),
                const SizedBox(width: 10),
                Text(
                  handleTime(task.startTime, task.endTime),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 7,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.description,
                  size: 15,
                  color: Colors.white,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Text(
                    task.description,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    left: 10,
                    right: 10,
                    top: 5,
                    bottom: 5,
                  ),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Text(
                    task.name,
                    style: TextStyle(
                      color: Utils.stringToColor(task.color),
                    ),
                  ),
                ),
                PopupMenuButton<Choice>(
                  onSelected: onTap,
                  color: Colors.white,
                  itemBuilder: (BuildContext context) {
                    return choices.map((Choice choice) {
                      return PopupMenuItem<Choice>(
                        value: choice,
                        child: Text(choice.title),
                      );
                    }).toList();
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  String handleTime(String startTime, String endTime) {
    int hourStart = int.parse(startTime.split(":")[0]);
    int hourEnd = int.parse(endTime.split(":")[0]);
    int milStart = int.parse(startTime.split(":")[1]);
    int milEnd = int.parse(endTime.split(":")[1]);
    if (milStart > milEnd) {
      milEnd += 60;
      hourEnd -= 1;
    }
    return ((hourEnd - hourStart) + ((milEnd - milStart) / 60))
            .toStringAsFixed(2) +
        " hours";
  }
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Todo', value: 0),
  const Choice(title: 'Doing', value: 1),
  const Choice(title: 'Done', value: 2),
];

import 'package:flutter/material.dart';

class BackItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.arrow_back_ios,
        size: 20,
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }
}

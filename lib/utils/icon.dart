import 'package:flutter/material.dart';

class IconUtils {
  static final todoIcon = Icons.history;
  static final Icon doingIcon = Icon(Icons.wb_incandescent);
  static final Icon doneIcon = Icon(Icons.check);
}

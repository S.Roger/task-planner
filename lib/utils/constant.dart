class Constants {
  static const int todo = 0;
  static const int doing = 1;
  static const int done = 2;
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Utils {
  static String getDateTime(DateTime dateTime) {
    return dateTime.day.toString() +
        "/" +
        dateTime.month.toString() +
        "/" +
        dateTime.year.toString();
  }

  static String getTime(TimeOfDay time) {
    return time.hour.toString() + ":" + time.minute.toString();
  }

  static String colorToString(Color color) {
    String colorString = color.toString();
    String valueString = colorString.split('(0x')[1].split(')')[0];
    return "0x" + valueString;
  }

  static Color stringToColor(String color) {
    return Color(int.parse(color));
  }

  static String dayOfWeek(String dateTime) {
    DateTime date = DateTime(int.parse(dateTime.split("/")[2]),
        int.parse(dateTime.split("/")[1]), int.parse(dateTime.split("/")[0]));
    switch (date.weekday) {
      case 1:
        return "Thứ 2";
      case 2:
        return "Thứ 3";
      case 3:
        return "Thứ 4";
      case 4:
        return "Thứ 5";
      case 5:
        return "Thứ 6";
      case 6:
        return "Thứ 7";
      case 7:
        return "CN";
    }
    return "";
  }

  static String dateTodayOfWeek(DateTime date) {
    switch (date.weekday) {
      case 1:
        return "Thứ 2";
      case 2:
        return "Thứ 3";
      case 3:
        return "Thứ 4";
      case 4:
        return "Thứ 5";
      case 5:
        return "Thứ 6";
      case 6:
        return "Thứ 7";
      case 7:
        return "Chủ nhật";
    }
    return "";
  }

  static String timeToday() {
    DateTime date = DateTime.now();

    return Utils.dateTodayOfWeek(date) +
        ", " +
        date.day.toString() +
        "/" +
        date.month.toString() +
        "/" +
        date.year.toString();
  }
}

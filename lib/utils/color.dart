import 'package:flutter/material.dart';

class ColorUtils {
  static final blueColor = 0xff3F47F4;
  static final Color todoColor = Color(0xffe46472);
  static final Color doingColor = Color(0xfff8bf7c);
  static final Color doneColor = Color(0xff6488e4);
}

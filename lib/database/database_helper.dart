import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final _databaseName = "data.db";
  static final _databaseVersion = 3;

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    print(path);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    print("----- CREATE TABLE -----");
    await db.execute('''
          CREATE TABLE user (
            id INTEGER PRIMARY KEY,
            username TEXT NOT NULL,
            description TEXT NOT NULL,
            avatar TEXT NOT NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE category (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            color TEXT NOT NULL,
            user_id INTEGER,
            FOREIGN KEY(user_id) REFERENCES user(id)
          )
          ''');
    await db.execute('''
      CREATE TABLE task (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            date TEXT NOT NULL,
            startTime TEXT NOT NULL,
            endTime TEXT NOT NULL,
            description TEXT NOT NULL,
            category_id INTEGER,
            status INTERGER DEFAULT 0,
            user_id INTEGER,
            FOREIGN KEY(category_id) REFERENCES category(id),
            FOREIGN KEY(user_id) REFERENCES user(id)
          )
    ''');
    print("----- END CREATE TABLE -----");
  }

  Future<int> insert(Map<String, dynamic> row, String table) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
    Database db = await instance.database;
    return await db.query(table);
  }

  Future<int> queryRowCount(String table) async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $table'));
  }

  Future<int> update(Map<String, dynamic> row, String table) async {
    Database db = await instance.database;
    int id = row['id'];
    print(row);
    return await db.update(table, row, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> delete(int id, String table) async {
    Database db = await instance.database;
    return await db.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  Future<List<Map<String, dynamic>>> queryAllRowsWhere(String table) async {
    Database db = await instance.database;
    // return await db.query(table);
    return await db.rawQuery(
        "SELECT DISTINCT date from task order by strftime('%d-%m-%Y %H:%M:%S'," +
            "date" +
            ") ASC");
  }

  Future<List<Map<String, dynamic>>> queryAllWhere(
      String table, String date) async {
    Database db = await instance.database;
    // return await db.rawQuery(
    //     "SELECT * from $table WHERE date = '$date' INNER JOIN category on category.id = $table.category_id");
    return await db.rawQuery(
        "SELECT task.title, task.date, task.startTime, task.endTime, task.description, task.status, task.id, category.name, category.color, task.category_id from $table INNER JOIN category on category.id = $table.category_id WHERE date = '$date'");
  }

  Future<List<Map<String, dynamic>>> getTaskByDay(int status) async {
    Database db = await instance.database;
    // return await db.rawQuery(
    //     "SELECT * from $table WHERE date = '$date' INNER JOIN category on category.id = $table.category_id");
    DateTime today = DateTime.now();
    String date = today.day.toString() +
        "/" +
        today.month.toString() +
        "/" +
        today.year.toString();
    return await db.rawQuery(
        "SELECT task.title, task.date, task.startTime, task.endTime, task.description, task.status, task.id, category.name, category.color, task.category_id from task INNER JOIN category on category.id = task.category_id WHERE date = '$date' AND task.status = $status");
  }
}

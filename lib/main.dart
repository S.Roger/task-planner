import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/category/category_bloc.dart';
import 'package:task_planner/blocs/category/category_state.dart';
import 'package:task_planner/blocs/task/task_bloc.dart';
import 'package:task_planner/blocs/task/task_state.dart';
import 'package:task_planner/blocs/user/user_bloc.dart';
import 'package:task_planner/blocs/user/user_state.dart';
import 'package:task_planner/screens/auth/sign_up.dart';
import 'package:task_planner/helper/notify_helper.dart';

import 'database/database_helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("init database");
    DatabaseHelper.instance.database;
    NotifiCationHelper().setUpNotify();
    return MultiBlocProvider(
      providers: [
        BlocProvider<CategoryBloc>(
          create: (_) => CategoryBloc(CategoryInitialized(), []),
        ),
        BlocProvider<TaskBloc>(
          create: (_) => TaskBloc(TaskStateInitial()),
        ),
        BlocProvider<UserBloc>(
          create: (_) => UserBloc(UserInitial()),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColorLight: Color(0xff3F47F4),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: NavigatorHomeScreen(),
        home: SignUp(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/user/user_event.dart';
import 'package:task_planner/blocs/user/user_state.dart';
import 'package:task_planner/database/database_helper.dart';
import 'package:task_planner/models/user.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc(UserState initialState) : super(initialState);

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is UserCheckLogin) {
      yield* _checkLogin(event);
    }
  }

  Stream<UserState> _checkLogin(event) async* {
    List<Map<String, dynamic>> list =
        await DatabaseHelper.instance.queryAllRows("user");
    if (list.length == 0) {
      yield UserLoginError();
    } else {
      yield UserLoginSuccess(UserModel.fromJson(list[0]));
    }
  }
}

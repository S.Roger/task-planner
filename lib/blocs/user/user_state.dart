import 'package:equatable/equatable.dart';
import 'package:task_planner/models/user.dart';

abstract class UserState extends Equatable {
  @override
  List<Object> get props => [];
  UserModel get user => null;
}

class UserInitial extends UserState {}

class UserLoginSuccess extends UserState {
  final UserModel user;

  UserLoginSuccess(this.user);
}

class UserLoginError extends UserState {}

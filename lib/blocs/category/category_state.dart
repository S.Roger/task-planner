import 'package:equatable/equatable.dart';

import '../../models/category.dart';

abstract class CategoryState extends Equatable {
  @override
  List<CategoryModel> get props => [];
}

class CategoryInitialized extends CategoryState {}

class CategorySuccess extends CategoryState {}

class CategoryError extends CategoryState {}

class CategoryListStateSuccess extends CategoryState {
  final List<CategoryModel> categories;

  CategoryListStateSuccess(this.categories);
  @override
  List<CategoryModel> get props => categories;

  @override
  String toString() => 'CategoryList successfully { categories: $categories }';
}

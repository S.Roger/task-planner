import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/category/category_event.dart';
import 'package:task_planner/blocs/category/category_state.dart';
import 'package:task_planner/database/database_helper.dart';
import '../../models/category.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  final List<CategoryModel> categories;
  CategoryBloc(CategoryState initialState, this.categories)
      : super(initialState);

  @override
  Stream<CategoryState> mapEventToState(CategoryEvent event) async* {
    if (event is CategoryList) {
      yield* _mapCategoryList();
    }
    if (event is CategoryAdd) {
      yield* _mapCategoryAdd(event);
    }
    if (event is CategoryEdit) {
      yield* _mapCategoryEdit(event);
    }
    if (event is CategoryDelete) {
      yield* _mapCategoryDelete(event);
    }
  }

  Stream<CategoryState> _mapCategoryList() async* {
    try {
      List<Map<String, dynamic>> data =
          await DatabaseHelper.instance.queryAllRows("category");
      List<CategoryModel> categories = [];
      data.forEach((element) {
        categories.add(CategoryModel.fromJson(element));
      });
      yield CategoryListStateSuccess(categories);
    } catch (_) {
      print("Server Errrors");
    }
  }

  Stream<CategoryState> _mapCategoryAdd(event) async* {
    try {
      await DatabaseHelper.instance.insert(event.category.toJson(), "category");
      yield CategoryInitialized();
    } catch (e) {
      print("Server Errrors ");
    }
  }

  Stream<CategoryState> _mapCategoryEdit(event) async* {
    try {
      await DatabaseHelper.instance.update(event.category.toJson(), "category");
      yield CategoryInitialized();
    } catch (e) {
      print("Server Errrors ");
    }
  }

  Stream<CategoryState> _mapCategoryDelete(event) async* {
    try {
      await DatabaseHelper.instance.delete(event.category.id, "category");
      yield CategoryInitialized();
    } catch (e) {
      print("Server Errrors ");
    }
  }
}

import 'package:equatable/equatable.dart';

import '../../models/category.dart';

abstract class CategoryEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CategoryAdd extends CategoryEvent {
  final CategoryModel category;
  CategoryAdd(this.category);

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'Category ADD { add: ${category.toJson()}}';
}

class CategoryEdit extends CategoryEvent {
  final CategoryModel category;
  CategoryEdit(this.category);

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'Category Edit { edit: ${category.toJson()}}';
}

class CategoryDelete extends CategoryEvent {
  final CategoryModel category;
  CategoryDelete(this.category);

  @override
  List<Object> get props => [category];

  @override
  String toString() => 'Category delete { add: ${category.toJson()}}';
}

class CategoryList extends CategoryEvent {}

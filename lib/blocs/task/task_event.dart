import 'package:equatable/equatable.dart';
import 'package:task_planner/models/task.dart';

class TaskEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class TaskAdd extends TaskEvent {
  final TaskModel task;

  TaskAdd(this.task);
}

class TaskEdit extends TaskEvent {}

class TaskDelete extends TaskEvent {}

class TaskList extends TaskEvent {
  final String date;

  TaskList(this.date);
}

class TaskCalendarList extends TaskEvent {}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task_planner/blocs/task/task_event.dart';
import 'package:task_planner/blocs/task/task_state.dart';
import 'package:task_planner/database/database_helper.dart';
import 'package:task_planner/helper/notify_helper.dart';
import 'package:task_planner/models/calendar.dart';
import 'package:task_planner/models/task.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  TaskBloc(TaskState initialState) : super(initialState);

  @override
  Stream<TaskState> mapEventToState(TaskEvent event) async* {
    if (event is TaskList) {
      // yield* _mapTaskList();
      yield* _mapTaskList(event);
    }
    if (event is TaskAdd) {
      yield* _mapTaskAdd(event);
    }
    if (event is TaskCalendarList) {
      yield* _mapCalenderList();
    }
  }

  Stream<TaskState> _mapTaskList(event) async* {
    List<Map<String, dynamic>> data =
        await DatabaseHelper.instance.queryAllWhere("task", event.date);
    List<TaskModel> tasks = [];
    data.forEach((element) {
      print(element);
      tasks.add(TaskModel.fromJson(element));
    });
    yield TaskListState(tasks);
  }

  Stream<TaskState> _mapTaskAdd(event) async* {
    final data = {
      "title": event.task.title,
      "date": event.task.date,
      "startTime": event.task.startTime,
      "endTime": event.task.endTime,
      "description": event.task.description,
      "category_id": event.task.categoryId,
      "status": 0
    };
    final result = await DatabaseHelper.instance.insert(data, "task");
    print(event.task.date);
    DateTime dateTime = DateTime(
      int.parse(event.task.date.split("/")[2]),
      int.parse(event.task.date.split("/")[1]),
      int.parse(event.task.date.split("/")[0]),
      int.parse(event.task.startTime.split(":")[0]),
      int.parse(event.task.startTime.split(":")[1]),
    );
    print(dateTime);
    await NotifiCationHelper().scheduleNotification(
      id: result,
      title: "Sắp đến giờ ${data['title']}",
      content: data['content'],
      dateTime: dateTime,
    );
    yield TaskStateInitial();
  }

  Stream<TaskState> _mapCalenderList() async* {
    List<Map<String, dynamic>> data =
        await DatabaseHelper.instance.queryAllRowsWhere("task");
    List<CalendarModel> calendars = [];
    data.forEach((element) {
      calendars.add(CalendarModel.fromJson(element));
    });
    yield TaskCalendarInit(calendars);
  }
}

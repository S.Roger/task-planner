import 'package:equatable/equatable.dart';
import 'package:task_planner/models/calendar.dart';
import 'package:task_planner/models/task.dart';

abstract class TaskState extends Equatable {
  @override
  List<TaskModel> get props => [];
  List<TaskModel> get tasks => [];
  List<CalendarModel> get calendar => [];
}

class TaskStateInitial extends TaskState {}

class TaskListState extends TaskState {
  final List<TaskModel> task;

  TaskListState(this.task);
  @override
  List<TaskModel> get tasks => task;

  @override
  String toString() => 'Task successfully { task: $task }';
}

class TaskCalendarInit extends TaskState {
  final List<CalendarModel> calendars;

  TaskCalendarInit(this.calendars);
  @override
  List<CalendarModel> get calendar => calendars;
  @override
  String toString() => 'Task calendar { task: $calendars }';
}
